CREATE DATABASE my_new_base;



CREATE TABLE IF NOT EXISTS address(
	id serial PRIMARY KEY, 
	street VARCHAR(50) NOT NULL, 
	country VARCHAR(50) NOT NULL, 
	city VARCHAR(50) NOT NULL, 
	zip_code VARCHAR(5) NOT NULL,
	UNIQUE(street, country, city, zip_code)
);

INSERT INTO address (street, country, city, zip_code) 
VALUES('Izvestkovaya', 'Ukraine', 'Kushugum', 70435),
	('Soborniy', 'Ukraine', 'Zaporozhye', 69000),
	('Mira', 'Ukraine', 'Balabino', 69000),
	('Gvardeyskiy', 'Ukraine', 'Zaporozhye', 69000);

CREATE TABLE IF NOT EXISTS location(
	id serial PRIMARY KEY, 
	name VARCHAR(50), 
	score INT,
	address_id INT REFERENCES address(id) ON DELETE CASCADE,
	UNIQUE(address_id),
	UNIQUE(name, score, address_id)
);

INSERT INTO location (name, score, address_id) 
VALUES('loc743', 456, 1),
		('loc951', 987, 2),
		('loc982', 357, 3),
		('loc982', 357, 4);

SELECT * FROM address LEFT JOIN location ON address.id = location.address_id;




CREATE TABLE IF NOT EXISTS user_data(
	id serial PRIMARY KEY,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	age SMALLINT,
	UNIQUE(first_name, last_name, age)
);

INSERT INTO user_data (first_name, last_name, age) 
VALUES('Ruslan', 'Nazarenko', 41),
	('Dima', 'Chernov', 25),
	('Ivan', 'Sidorov', 18);
	
CREATE TABLE IF NOT EXISTS address_user_data_relation(
address_id INT REFERENCES address(id),
user_id  INT REFERENCES user_data(id),
	UNIQUE (address_id, user_id)
);
	
INSERT INTO address_user_data_relation (address_id, user_id) 
VALUES(1, 2),
(1, 3),
(3, 1),
(2, 2),
(3, 2);

SELECT user_data.first_name, user_data.last_name, address.street FROM user_data
      LEFT JOIN address_user_data_relation ON user_data.id = address_user_data_relation.user_id
      LEFT JOIN address ON address_user_data_relation.address_id = address.id
